#include <algorithm>
#include <array>
#include <execution>
#include <iostream>

#include <gsl/span>

void test(gsl::span<int> span)
{
  std::transform(std::execution::par_unseq, span.begin(), span.end(), span.begin(), [](int i) { return (i + 1); });
}

int main(void)
{
  std::array arr{ 1, 2, 3 };

  test(arr);

  for (int i : arr)
  {
    std::cout << i << '\n';
  }

  return 0;
}